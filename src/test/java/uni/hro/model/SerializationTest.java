package uni.hro.model;

import de.mein.core.serialize.deserialize.entity.SerializableEntityDeserializer;
import de.mein.core.serialize.exceptions.JsonDeserializationException;
import de.mein.core.serialize.exceptions.JsonSerializationException;
import de.mein.core.serialize.serialize.fieldserializer.entity.SerializableEntitySerializer;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by xor on 5/13/17.
 */
public class SerializationTest {
    @Test
    public void serializeFish() throws JsonSerializationException, IllegalAccessException, JsonDeserializationException {
        World world = new World(400, 200);
        Fish fish = new Fish(world, 5, 6);
        String json = SerializableEntitySerializer.serialize(fish);
        System.out.println(json);
        Fish deserialized = (Fish) SerializableEntityDeserializer.deserialize(json);
        String jsonAgain = SerializableEntitySerializer.serialize(deserialized);
        assertEquals(json, jsonAgain);
        assertEquals(5, fish.x,0.00001);
        assertEquals(6, fish.y,0.00001);
    }
}
