package uni.hro.model;

import de.mein.auth.socket.process.val.MeinValidationProcess;
import de.mein.auth.tools.N;
import de.mein.auth.tools.WaitLock;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;
import org.junit.Test;
import uni.hro.networking.AquaClient;
import uni.hro.networking.AquaServer;

/**
 * Created by xor on 5/15/17.
 */
public class ConnectionTest {
    /**
     * achtung! test hält nie an
     * @throws Exception
     */
    @Test
    public void connect() throws Exception {
        AquaServer server = new AquaServer(AquaServer.DEFAULT_SERVER_DIRECTORY, "fish.server", 8888, 8889);
        AquaClient client = new AquaClient(AquaClient.DEFAULT_CLIENT_DIRECTORY, "fish.client", 8890, 8891);

        server.startAqua();
        client.startAqua();
        Promise<MeinValidationProcess, Exception, Void> connected = client.getMeinAuthService().connect(null, "localhost", 8888, 8889, true);
        connected.done(val -> N.r(() -> {
            Promise<Void, Void, Void> registered = client.getAquaService().regAsClient("localhost", 8888, 8889);
            registered.done(result -> N.r(() -> {
                server.getAquaService().startSimulation();
            }));
        }));
        WaitLock waitLock = new WaitLock().lock().lock();
        System.out.println("ConnectionTest.connect.END");

    }
}
