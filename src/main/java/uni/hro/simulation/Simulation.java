package uni.hro.simulation;

import com.anji.integration.ActivatorTranscriber;
import com.anji.neat.Evolver;
import com.anji.util.Properties;
import org.jgap.Chromosome;
import uni.hro.model.*;

import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by xor on 5/15/17.
 */
public class Simulation {
    private final int noOfKraken;
    private final int noOfFishes;
    private final int aquariumWidth;
    private final int aquariumHeight;
    private ActivatorTranscriber factory;
    private World world;
    private Evolver fishEvolver, krakenEvolver;
    private ExecutorService executor;
    private final int noOfCores;
    private int stepNumber;
    private Set<WorldEntity> allCreatures;

    public Simulation(Integer numberOfFishes, Integer numberOfKraken, int numberOfPlants, int numberOfStones, int aquariumWidth, int aquariumHeight) {
        this.world = World.createRandomWorld1(numberOfFishes, numberOfKraken, numberOfPlants, numberOfStones, aquariumWidth, aquariumHeight);
        noOfFishes = numberOfFishes;
        noOfKraken = numberOfKraken;
        this.aquariumWidth = aquariumWidth;
        this.aquariumHeight = aquariumHeight;
        noOfCores = Runtime.getRuntime().availableProcessors();
        try {
            executor = Executors.newFixedThreadPool(noOfCores);
            Properties props = new Properties();
            InputStream in = getClass().getResourceAsStream("/uni/hro/nn/fish.properties");
            props.load(in);
            in.close();
            props.setProperty("stimulus.size",Fish.calcInputSize().toString());
            props.setProperty("popul.size",numberOfFishes.toString());
            fishEvolver = Evolver.instance(props);
            in = getClass().getResourceAsStream("/uni/hro/nn/kraken.properties");
            props = new Properties();
            props.load(in);
            in.close();
            props.setProperty("stimulus.size",Kraken.calcInputSize().toString());
            props.setProperty("popul.size",numberOfKraken.toString());
            krakenEvolver = Evolver.instance(props);
            factory = (ActivatorTranscriber) props.singletonObjectProperty(ActivatorTranscriber.class);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void evolve() {

        fishEvolver.evolveGeneration();
    }


    private Hashtable<Creature, Void> deadStuff = new Hashtable<>();

    public boolean simulate() {
        for (int i = 0; i < 1; i++) {
            if (new Random().nextFloat() > 0.98)
                world.getEntities().add(new Food(world, World.generateRandom(20, world.getWidth() - 20), 1));
        }
        WorldEntity[] entities = world.getEntities().toArray(new WorldEntity[0]);
        return process(entities);
    }

    private boolean process(WorldEntity[] entities) {
    	stepNumber++;
        boolean everythingIsDead = true;
        List<WorldEntity> probablyExistingStuff = new ArrayList<>(entities.length);
        try {
            int noOfThreads = noOfCores;
            int size = entities.length;
            if (size < noOfThreads)
                noOfThreads = size;
            if (size < 1)
                return true;
            int width = size / noOfThreads;
            int rest = size - noOfThreads * width;
            int startIndex = 0;
            int endIndex = width;
            List<SimulationCallable> callables = new ArrayList<>();
            for (int i = 0; i < noOfThreads; i++) {
                if (rest > 0) {
                    endIndex++;
                    rest--;
                }
                SimulationCallable callable = new SimulationCallable(entities, startIndex, endIndex, factory);
                callables.add(callable);
                startIndex = endIndex;
                endIndex += width;
            }
            List<Future<List<WorldEntity>>> futures = executor.invokeAll(callables);
            for (Future<List<WorldEntity>> future : futures) {
                List<WorldEntity> futureResult = future.get();
                if (futureResult != null) {
                    probablyExistingStuff.addAll(futureResult);
                }
            }
            // remove consumed WorldEntities
            world.getEntities().clear();
            Set<WorldEntity> existingStuff = world.getEntities();
            for (WorldEntity worldEntity : probablyExistingStuff) {
                if (worldEntity instanceof Creature) {
                    Creature creature = (Creature) worldEntity;
                    if (creature.isAlive())
                        everythingIsDead = false;
                    else {
                        //creature.setFitnessValue(1);
                    }
                }
                if (worldEntity instanceof Consumable) {
                    Consumable consumable = (Consumable) worldEntity;
                    if (!consumable.isConsumed())
                        existingStuff.add(consumable);
                } else {
                    existingStuff.add(worldEntity);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        updateFitnessValues(entities);
        
        return everythingIsDead;
    }


    public World getWorld() {
        return world;
    }

    public void prepare() {
    	stepNumber = 0;
    	
        List<Chromosome> fishChroms = fishEvolver.nextGeneration();
        List<Chromosome> krakenChroms = krakenEvolver.nextGeneration();
        int fishCount = 0;
        int krakenCount = 0;
        
        Set<WorldEntity> entities = new HashSet<>(world.getEntities());
        world.getEntities().clear();
        for (WorldEntity worldEntity : entities) {
            if (worldEntity instanceof Food) {

            } else if ((worldEntity instanceof Creature)) {

            } else {
                world.getEntities().add(worldEntity);
            }
        }
        for (int i = 0; i < noOfFishes; i++) {
            Fish fish = new Fish(world, random(aquariumWidth), random(aquariumHeight));
            fish.setChromosome(fishChroms.get(fishCount++));
            world.getEntities().add(fish);
        }
        for (int i = 0; i < noOfKraken; i++) {
            Kraken kraken = new Kraken(world, random(aquariumWidth), random(aquariumHeight));
            kraken.setChromosome(krakenChroms.get(krakenCount++));
            world.getEntities().add(kraken);
        }
    }

    private Random random = new Random();

    private float random(float range) {
        return random.nextFloat() * range;
    }
    
    /**
     * Updates the fitness value for every creature if it has not yet been set and the creature has died.
     */
    private void updateFitnessValues(WorldEntity[] entities) {
    	for (WorldEntity worldEntity : entities) {
    		//if (worldEntity instanceof Creature) System.out.println(worldEntity.getClass()+"\tlebt: "+((Creature) worldEntity).isAlive()+"\tmit Fitness: "+((Creature) worldEntity).getFitnessValue());
    		if (worldEntity instanceof Creature 
    				&& ((Creature) worldEntity).getFitnessValue() == 0
    				&& !((Creature) worldEntity).isAlive()) {
    			((Creature) worldEntity).setFitnessValue(stepNumber);
    		}
    	}
    	//System.out.println("#######################");
    }
}
