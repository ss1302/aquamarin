package uni.hro.model;

import java.awt.*;
import java.util.Random;

/**
 * Created by Er Bü on 10.05.2017.
 */
public class Food extends Consumable {

	protected World world;
	private double sinkspeed;
	private float fuel = 25;

	public Food(World world, float x, float y) {
		super(x, y, 2, 2);
		this.world = world;
		this.sinkspeed = ((double)generateRandom(2, 7))/10;
		//this.sinkspeed=0.3;
	}

	public boolean sink() {
		dy = (float) sinkspeed;
		y = y + dy;
		
		if (this.y > world.getHeight()){
		return false;
		}
		
		return true;		
	}
	
	
	public synchronized float emptyFuel() {
		float result = fuel;
		fuel = 0;
		return result;
	}


	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}


}
