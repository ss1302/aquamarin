package uni.hro.model.neurons;

import uni.hro.model.Food;

/**
 * Created by xor on 5/29/17.
 */
public class FoodInputNeuron extends SecondaryNeuron<Food> {
    public FoodInputNeuron(InputNeuron inputNeuron, Class<Food> clazz) {
        super(inputNeuron, clazz);
    }
}
