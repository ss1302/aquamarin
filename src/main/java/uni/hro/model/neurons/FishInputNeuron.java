package uni.hro.model.neurons;

import uni.hro.model.Fish;

/**
 * Created by xor on 5/23/17.
 */
public class FishInputNeuron extends SecondaryNeuron<Fish> {

    public FishInputNeuron(InputNeuron inputNeuron, Class<Fish> clazz) {
        super(inputNeuron, clazz);
    }
}
