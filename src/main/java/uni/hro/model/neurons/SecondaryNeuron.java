package uni.hro.model.neurons;

import uni.hro.model.WorldEntity;

/**
 * Created by xor on 5/23/17.
 */
public abstract class SecondaryNeuron<T> extends InputNeuron {
    protected final InputNeuron inputNeuron;
    private final Class<T> clazz;

    public SecondaryNeuron(InputNeuron inputNeuron, Class<T> clazz) {
        this.inputNeuron = inputNeuron;
        this.clazz = clazz;
    }

    public double getValue() {
        for (WorldEntity worldEntity : inputNeuron.getPerceivedEntities()) {
            if (worldEntity.getClass().equals(clazz))
                return 1.0;
        }
        return 0;
    }
}
