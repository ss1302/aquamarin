package uni.hro.model.neurons;

import uni.hro.model.Creature;
import uni.hro.model.IntegerPoint;
import uni.hro.model.WorldEntity;

import java.util.HashSet;
import java.util.Set;

public class InputNeuron {


    private IntegerPoint offset;
    private Creature creature;
    private Set<WorldEntity> perceivedEntities = new HashSet<>();

    public InputNeuron(Creature creature, IntegerPoint offset) {
        this.creature = creature;
        this.offset = offset;
    }

    public InputNeuron() {
    }


    public void clear() {
        perceivedEntities.clear();
    }

    public InputNeuron addPerceivedEntity(WorldEntity entity) {
        perceivedEntities.add(entity);
        return this;
    }


    public final static float HALF_WIDTH = 8;
    public final static float WIDTH = HALF_WIDTH * 2;


    public int getLeftX() {
        float leftX = creature.getX() - HALF_WIDTH;
        float delta = offset.getX() * WIDTH;
        if (creature.looksLeft())
            delta *= -1;
        return (int) (leftX + delta);
    }

    public int getRightX() {
        float leftX = creature.getX() + HALF_WIDTH;
        float delta = offset.getX() * WIDTH;
        if (creature.looksLeft())
            delta *= -1;
        return (int) (leftX + delta);
    }

    public int getTopY() {
        float y = creature.getY() - HALF_WIDTH;
        float delta = offset.getY() * WIDTH;
        return (int) (y + delta);
    }

    public int getBottomY() {
        float y = creature.getY() + HALF_WIDTH;
        float delta = offset.getY() * WIDTH;
        return (int) (y + delta);
    }

    public Set<WorldEntity> getPerceivedEntities() {
        return perceivedEntities;
    }
}
