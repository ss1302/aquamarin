package uni.hro.model;

/**
 * Created by Er Bü on 15.04.2017.
 */
public class Fish extends Creature {


    public Fish(World world, float x, float y) {
        super(world, 100f, x, y, 30, 13);
        createNeuron(1, 0);
        createNeuron(1, 1);
        createNeuron(1, -1);
        createNeuron(2, 0);
        createNeuron(2, 1);
        createNeuron(2, 2);
        createNeuron(2, -1);
        createNeuron(2, -2);
        createNeuron(3, 0);
        createNeuron(3, 1);
        createNeuron(3, 2);
        createNeuron(3, 3);
        createNeuron(3, -1);
        createNeuron(3, -2);
        createNeuron(3, -3);
        createNeuron(4,0);
        createNeuron(4,1);
        createNeuron(4,2);
        createNeuron(4,3);
        createNeuron(4,4);
        createNeuron(4,-1);
        createNeuron(4,-2);
        createNeuron(4,-3);
        createNeuron(4,-4);
        createNeuron(5,0);
        createNeuron(5,1);
        createNeuron(5,2);
        createNeuron(5,3);
        createNeuron(5,4);
        createNeuron(5,-1);
        createNeuron(5,-2);
        createNeuron(5,-3);
        createNeuron(5,-4);
        createNeuron(0, 1);
        createNeuron(0, -1);
        createNeuron(-1, 0);
        createNeuron(-1, -1);
        createNeuron(-1, 1);
        createSecondaryNeurons();
    }

    public static Integer calcInputSize(){
        Fish fish = new Fish(new World(1,1),1,1);
        return Creature.calcInputSize(fish);
    }

    public Fish() {

    }

    @Override
    public void handleCollisions() {
        for (WorldEntity worldEntity : collidedEntities) {
            if (worldEntity instanceof Food) {
                Food food = (Food) worldEntity;
                addFuel(food.consume());
            }
        }
    }


    @Override
    public synchronized float consume() {
        if (!consumed) {
            consumed = true;
            if (isAlive())
                return fuel;
            return 25f;
        }
        return 0f;
    }
}
