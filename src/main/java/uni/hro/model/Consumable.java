package uni.hro.model;

/**
 *
 * Stuff that can be eaten by Creatures.
 * Created by xor on 5/30/17.
 */
public abstract class Consumable extends WorldEntity {

    public Consumable(float x, float y, float width, float height) {
        super(x, y, width, height);
    }
    public Consumable(){

    }

    protected boolean consumed = false;

    public boolean isConsumed() {
        return consumed;
    }

    /**
     * consumes this Entity.
     * @return
     */
    public synchronized float consume() {
        if (!consumed) {
            consumed = true;
            return 25f;
        }
        return 0f;
    }

    public void setConsumed(boolean consumed) {
        this.consumed = consumed;
    }
}
