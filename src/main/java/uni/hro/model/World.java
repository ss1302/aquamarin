package uni.hro.model;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Er Bü on 14.04.2017.
 */
public class World {
    private final int width; 
    private final int height;
    private Set<WorldEntity> entities = new HashSet<>();

    public World(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public static World createRandomWorld1(int numberOfFishes, int numberOfKraken, int numberOfPlants, int numberOfStones, int displayWidth, int displayHeight) {
        World world = new World(displayWidth, displayHeight);
        int iterator1 = 0;
        while (iterator1 < numberOfFishes) {
            int newYValue = generateRandom(displayHeight - 30, displayHeight - 10);

            world.getEntities().add(new Fish(world, generateRandom(20, displayWidth - 100), generateRandom(30, displayHeight - 30)));
            iterator1++;
        }


        int iterator3 = 0;
        while (iterator3 < numberOfKraken) {
            int newYValue = generateRandom(displayHeight - 30, displayHeight - 10);

            world.getEntities().add(new Kraken(world, generateRandom(20, displayWidth - 100), generateRandom(30, displayHeight - 30)));
            iterator3++;
        }

        int iterator = 0;
        int oldYValue = 0;
        while (iterator < numberOfStones) {
            int newYValue = generateRandom(displayHeight - 65, displayHeight - 55);
            if (oldYValue < newYValue) {
                world.getEntities()
                        .add(new Stone(world, generateRandom(20, displayWidth - 150), newYValue, 100, 50));
                iterator++;
            }
        }


        int iterator2 = 0;
        int oldYValue2 = 0;
        while (iterator2 < numberOfPlants) {
            int newYValue = generateRandom(displayHeight - 40, displayHeight - 30);
            if (oldYValue2 < newYValue) {
                world.getEntities().add(new Plant(world, generateRandom(20, displayWidth - 100),
                        newYValue, 100, 100));
                iterator2++;
            }
        }
        return world;
    }

    public static int generateRandom(int min, int max) {
        Random rn = new Random();
        int randomvalue = rn.nextInt(max - min + 1) + min;
        return randomvalue;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Set<WorldEntity> getEntities() {
        return entities;
    }

    public void checkEntityCollision(Creature a, WorldEntity b) {

        // checks the 4 conditions that must be true for a collision, then either bounces them back or in case of
        // fish x food, kraken x fish or kraken x food consumes the the second
        // it should be noted that this only works in a model were the lines are parallel to one another

        if (a.getLeftX() < b.getRightX() && a.getRightX() > b.getLeftX() && a.getTopY() < b.getBottomY()
                && a.getBottomY() > b.getTopY() && !(a.equals(b)))  {

            if (a instanceof Fish && b instanceof Fish) {
                bounceOff(a,b);
            } else if (a instanceof Fish && b instanceof Food) {
                entities.remove(b);
                a.addFuel(02);
            } else if (a instanceof Kraken && b instanceof Fish) {
                entities.remove(b);
                a.addFuel(02);
            } else if (a instanceof Kraken && b instanceof Food) {
                entities.remove(b);
            }

        }

    }

    public void bounceOff(WorldEntity a, WorldEntity b) {

        // checks velocity of two colliding Entities and pushes them in the opposite direction
        // not the most realistic solution, but it should work
        // TODO: needs to be improve

        if (a.vx > 0) {
            if (b.vx > 0) {
                a.vx = 10;
                b.vx = -10;
            } else {
                a.vx = -10;
                b.vx = 10;
            }
        } else {
            if (b.vx > 0) {
                a.vx = 10;
                b.vx = -10;
            } else {
                a.vx = -10;
                b.vx = 10;
            }
        }

        if (a.vy > 0) {
            if (b.vy > 0) {
                a.vy = 10;
                b.vy = -10;
            } else {
                a.vy = -10;
                b.vy = 10;
            }
        } else {
            if (b.vy > 0) {
                a.vy = 10;
                b.vy = -10;
            } else {
                a.vy = -10;
                b.vy = 10;
            }
        }
    }

}
