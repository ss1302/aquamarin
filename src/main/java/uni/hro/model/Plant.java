/**
 * 
 */
package uni.hro.model;

import uni.hro.client.PlantDrawer;
import uni.hro.client.StoneDrawer;

import java.awt.*;

/**
 * @author Columbus
 *
 */
public class Plant extends WorldEntity {

	protected World world;

	protected int[] xLeafGeometriePoints;
	protected int[] yLeafGeometriePoints;
	protected int numberofSegments;
	protected double moveDirectionandSpeed = 0.001;

	public double getMoveDirectionandSpeed() {
		return moveDirectionandSpeed;
	}

	public void setMoveDirectionandSpeed(double moveDirectionandSpeed) {
		this.moveDirectionandSpeed = moveDirectionandSpeed;
	}

	public int getNumberofSegments() {
		return numberofSegments;
	}

	public void setNumberofSegments(int numberofSegments) {
		this.numberofSegments = numberofSegments;
	}

	protected double currentAngle;

	public double getCurrentAngle() {
		return currentAngle;
	}

	public void setCurrentAngle(double currentAngle) {
		this.currentAngle = currentAngle;
	}

	public int[] getxLeafGeometriePoints() {
		return xLeafGeometriePoints;
	}

	public void setxLeafGeometriePoints(int[] xLeafGeometriePoints) {
		this.xLeafGeometriePoints = xLeafGeometriePoints;
	}

	public int[] getyLeafGeometriePoints() {
		return yLeafGeometriePoints;
	}

	public void setyLeafGeometriePoints(int[] yLeafGeometriePoints) {
		this.yLeafGeometriePoints = yLeafGeometriePoints;
	}

	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Plant(World world, float x, float y, float width, float height) {
		super(x, y, width, height);
		this.world = world;

		PlantDrawer plantdrawer = new PlantDrawer();
		this.xLeafGeometriePoints = plantdrawer.createRandomizedGeometrieValuesX();
		this.yLeafGeometriePoints = plantdrawer.createRandomizedGeometrieValuesY();
		plantdrawer = null;
		if (world.getHeight() > 500) {
			this.numberofSegments = plantdrawer.generateRandom(1, 10);
		} else {
			this.numberofSegments = plantdrawer.generateRandom(1, 6);
		}
		this.currentAngle = (double) (plantdrawer.generateRandom(0, 15)) / 100;
	}

	public void plantsInTheCurrentMovement() {
		double maxMovementAngle = 0.2;

		currentAngle = currentAngle + moveDirectionandSpeed;
		// System.out.println(currentAngle + " " + moveDirectionandSpeed);

		if (maxMovementAngle < currentAngle) {
			this.setMoveDirectionandSpeed(-0.0005);

		}

		if (-maxMovementAngle > currentAngle) {
			this.setMoveDirectionandSpeed(0.0005);

		}

	}


}
