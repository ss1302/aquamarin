package uni.hro.client;

import uni.hro.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;

/**
 * 
 * @author Joerg, Johann
 * 
 *         Das AquariumJPanel dient als Zeichenfl�che f�r das Aquarium und alle
 *         Lebewesen und Objekte, die sich darin befinden.
 *
 */
public class AquariumJPanel extends JPanel {

	/**
	 * Aquariumwasserfarbe
	 */
	Color backgroundColor = new Color(18, 120, 190);

	/**
	 * Groesse des Fensters
	 */
	int displayWidth;
	int displayHeight;

	/**
	 * Zur Ausgabe der Frames pro Sekunde
	 */
	int frames = 0;
	long firstFrame;
	long currentFrame;
	int fps;

	private int jumpNumberOfGenerations = 0;

	/**
	 * Popupmenu
	 */
	private JPopupMenu pop;

	private World world;

	/**
	 * Speichern Steine und Pflanzen separat.
	 */
	HashSet<Stone> setOfStones = new HashSet<Stone>();
	HashSet<Plant> setOfPlants = new HashSet<Plant>();

	public HashSet<Plant> getSetOfPlants() {
		return setOfPlants;
	}

	public void setSetOfPlants(HashSet<Plant> setOfPlants) {
		this.setOfPlants = setOfPlants;
	}

	private boolean fastforward;

	/**
	 * Konstruktur des AquariumJPanel
	 * 
	 * @param world
	 *            Die Welt, in der die Simulation stattfindet.
	 */
	public AquariumJPanel(World world) {
		this.displayWidth = world.getWidth();
		this.displayHeight = world.getHeight();
		this.world = world;
		this.pop = new JPopupMenu();
		menu();

		prepareDrawing();
		repaint();

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (pop.isVisible()) {
					pop.setVisible(false);
				} else {
					pop.setLocation(arg0.getX(), arg0.getY()); // get mouse
																// location
					pop.setVisible(true); // Show this popup menu
				}
			}
		});
	}

	/**
	 * Alle in der Welt vorkommenden Pflanzen und Steine werden separat
	 * gespeichert.
	 * 
	 */
	private void prepareDrawing() {

		for (Object s : new HashSet<>(world.getEntities())) {

			if (s instanceof Stone) {

				Stone newStone = (Stone) s;

				setOfStones.add(newStone);
			}

			if (s instanceof Plant) {

				Plant plant = (Plant) s;
				setOfPlants.add(plant);
			}

		}

	}

	/**
	 * Bestimmt den Aufbau des Popup-Menus
	 */
	public void menu() {
		JSeparator sep = new JSeparator();
		JMenuItem informationItem = new JMenuItem("Show Information (TODO)");
		JMenuItem jumpFive = new JMenuItem("Jump 5 Generations");
		JMenuItem jumpOneHundred = new JMenuItem("Jump 100 Generations (TODO)");
		JMenuItem jumpFiveHundred = new JMenuItem("Jump 500 Generations (TODO)");
		JMenuItem jumpOneThousand = new JMenuItem("Jump 1000 Generations (TODO)");
		JSeparator sep2 = new JSeparator();
		JMenuItem closeWindowItem = new JMenuItem("End Simulation");

		pop.add(informationItem);
		pop.add(sep);
		pop.add(jumpFive);
		pop.add(jumpOneHundred);
		pop.add(jumpFiveHundred);
		pop.add(jumpOneThousand);
		pop.add(sep2);
		pop.add(closeWindowItem);

		closeWindowItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				System.exit(0);
			}
		});

		jumpFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {

				fastforward=true;
				setJumpNumberOfGenerations(5);
				pop.setVisible(false);
			}
		});

		/**
		 * jumpOneHundred.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent ev) {
		 * fastforward=true;
		 * setJumpNumberOfGenerations(100); pop.setVisible(false); } });
		 * 
		 * jumpFiveHundred.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent ev) {
		 * fastforward=true;
		 * setJumpNumberOfGenerations(500); pop.setVisible(false); } });
		 * 
		 * jumpOneThousand.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent ev) {
		 * fastforward=true;
		 * setJumpNumberOfGenerations(1000); pop.setVisible(false); } });
		 **/

	}

	// @Override
	/**
	 * Gibt an, was auf das AquariumJPanel gezeichnet werden soll.
	 * 
	 * 
	 * 
	 */
	public void paintComponent(Graphics g) {

		super.paintComponent(g);

		setBackground(backgroundColor);

		// nun in paint() / update() bzw. paintComponent() ...
		frames++;
		currentFrame = System.currentTimeMillis();
		if (currentFrame > firstFrame + 1000) {
			firstFrame = currentFrame;
			fps = frames;
			frames = 0;
		}
		String fpss = "FPS: " + fps;
		g.drawString(fpss, 0, 10);

		/**
		 * Boden zeichnen
		 */
		GroundDrawer.draw(g, displayWidth, displayHeight, new Color(232, 235, 224), 1, 1);

		/**
		 * Zeichnen von Kreaturen u
		 */
		for (Object s : new HashSet<>(world.getEntities())) {

			if (s instanceof Fish) {
				Fish fish = (Fish) s;
				if (fish.isAlive()) {

					FishDrawer.draw(g, fish);
					HitboxDrawer.draw(g, fish);
				} else {
					DeadFishDrawer.draw(g, fish);
				}

			}

			if (s instanceof Kraken) {
				Kraken kraken = (Kraken) s;
				KrakenDrawer.draw(g, kraken);
				if (kraken.isAlive())
					HitboxDrawer.draw(g, kraken);
			}

			if (s instanceof Food) {
				Food food = (Food) s;
				FoodDrawer.draw(g, food);
			}
			/**
			 * if (s instanceof Plant) { Plant plant = (Plant) s;
			 * PlantDrawer.draw(g, plant); }
			 * 
			 * if (s instanceof Stone) { Stone stone = (Stone) s;
			 * StoneDrawer.draw(g, stone); }
			 */
		}

		/**
		 * Pflanzen und Steine werden aus separaten Listen abgerufen. Die
		 * Objekte in den Hashsets geraten ducheinander und wuerden anderenfalls
		 * auch immer in unterschiedlicher Reihenfolge gezeichnet werden => Mal
		 * waere ein Stein vorne, dann wieder eine Pflanze... usw.
		 */

		for (Stone stone : setOfStones) {

			StoneDrawer.draw(g, stone);
		}

		for (Plant plant : setOfPlants) {

			PlantDrawer.draw(g, plant);
		}

		if (fastforward == true) {
			
			
			FastForwardSymbolDrawer.draw(g, displayWidth, displayHeight);
			fastforward =false;
		}
	}

	/**
	 * 
	 * @return
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * 
	 * @return
	 */
	public int getJumpNumberOfGenerations() {
		return jumpNumberOfGenerations;
	}

	/**
	 * 
	 * @param jumpNumberOfGenerations
	 */
	public void setJumpNumberOfGenerations(int jumpNumberOfGenerations) {
		this.jumpNumberOfGenerations = jumpNumberOfGenerations;
	}

	public boolean isFastforward() {
		return fastforward;
	}

	public void setFastforward(boolean fastforward) {
		this.fastforward = fastforward;
	}

}
