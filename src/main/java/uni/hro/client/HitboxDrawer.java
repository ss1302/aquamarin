package uni.hro.client;

import uni.hro.model.Creature;
import uni.hro.model.Point;
import uni.hro.model.WorldEntity;
import uni.hro.model.neurons.InputNeuron;

import java.awt.*;
import java.util.Map;

/**
 * Created by xor on 6/2/17.
 */
public class HitboxDrawer {
	public static void draw(Graphics graphics, WorldEntity worldEntity) {
		Graphics2D g2 = (Graphics2D) graphics;

		
		// Hitbox
					final float x = worldEntity.getX();
					final float y = worldEntity.getY();
					final float halfWidth = worldEntity.getHalfWidth();
					final float halfHeight = worldEntity.getHalfHeight();
					
					{
						int xx = (int) (x - halfWidth);
						int yy = (int) (y - halfHeight);
						int ww = (int) (2 * halfWidth);
						int hh = (int) (2 * halfHeight);
						g2.drawRect(xx, yy, ww, hh);
					}
		
		if (worldEntity instanceof Creature) {
			
			Creature creature = (Creature)worldEntity;
			
			// Neuronen
			Map<InputNeuron, Point<Integer>> neurons = creature.getNeurons();
			g2.setColor(Color.gray);
			for (InputNeuron neuron : neurons.keySet()) {
				uni.hro.model.Point<Integer> nPoint = neurons.get(neuron);
				final float direction = (creature.looksLeft()) ? -1.0f : 1.0f;
				float fff = (y + (y * 2 * halfHeight) - halfHeight);
				final int neuronTopY = neuron.getTopY();
				final int neuronLeftX = neuron.getLeftX();
				if (neuron.getPerceivedEntities().size() > 0) {
					g2.setColor(Color.RED);
				} else
					g2.setColor(Color.gray);
				g2.drawRect(neuronLeftX, neuronTopY, (int) InputNeuron.WIDTH, (int) InputNeuron.WIDTH);
			}
		}
	}
}
