package uni.hro.client;

/**
 * 
 */

import javax.swing.*;

import uni.hro.model.Kraken;

import java.awt.*;
import java.awt.geom.*;

/**
 * @author Joerg
 *
 *         Diese Klasse bestimmt, wie ein Kraken gezeichnet wird.
 */

class KrakenDrawer {

	/**
	 * Diese Methode bestimmt, wie ein Kraken gezeichnet wird.
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * @param kraken
	 *            Der zu zeichnende Kraken
	 */
	public static void draw(Graphics g, Kraken kraken)

	{

		Graphics2D g2 = (Graphics2D) g;

		/**
		 * Anpassen der Position, damit der Kraken genau in der Hit-Box
		 * gezeichnet wird
		 */
		int positionX = (int) (kraken.getX() - 9);
		int positionY = (int) (kraken.getY() - 23);

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Kraken.
		 */
		double sizefactorX = 0.4;
		double sizefactorY = 0.4;
		Color color = kraken.getCurrentColor();

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Objekts
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);

		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/**
		 * Arm5
		 */
		int[] xPoints5 = { 20, 30, 60, 45, 45 };
		int[] yPoints5 = { 25, 70, 110, 70, 20 };
		int nPoints5 = 5;
		g2.setColor(color.darker());
		g2.fillPolygon(xPoints5, yPoints5, nPoints5);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints5, yPoints5, nPoints5);

		/**
		 * Arm4
		 */
		int[] xPoints4 = { 10, 10, 20, };
		int[] yPoints4 = { 25, 130, 20, };
		int nPoints4 = 3;
		g2.setColor(color);
		g2.fillPolygon(xPoints4, yPoints4, nPoints4);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints4, yPoints4, nPoints4);

		/**
		 * Arm 3
		 */
		int[] xPoints = { 25, 40, 40 };
		int[] yPoints = { 40, 115, 25 };
		int nPoints = 3;
		g2.setColor(color);
		g2.fillPolygon(xPoints, yPoints, nPoints);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints, yPoints, nPoints);

		/**
		 * Arm1
		 */
		int[] xPoints2 = { 10, 0, -30, 10, 25 };
		int[] yPoints2 = { 25, 70, 110, 70, 20 };
		int nPoints2 = 5;
		g2.setColor(color.darker());
		g2.fillPolygon(xPoints2, yPoints2, nPoints2);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints2, yPoints2, nPoints2);

		/**
		 *  Arm6
		 */
		int[] xPoints6 = { 17, 7, -13, 17, 32 };
		int[] yPoints6 = { 25, 70, 120, 70, 20 };
		int nPoints6 = 5;
		g2.setColor(color);
		g2.fillPolygon(xPoints6, yPoints6, nPoints6);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints6, yPoints6, nPoints6);

		/**
		 *  Arm2
		 */
		int[] xPoints3 = { 15, 25, 30, };
		int[] yPoints3 = { 25, 130, 20, };
		int nPoints3 = 3;
		g2.setColor(color);
		g2.fillPolygon(xPoints3, yPoints3, nPoints3);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints3, yPoints3, nPoints3);

		/**
		 *  Koerper
		 */
		g2.setColor(color.brighter());
		g2.fillOval(0, 0, 50, 50);
		g2.setColor(Color.BLACK);
		g2.drawOval(0, 0, 50, 50);

		/**
		 *  Auge1
		 */
		g2.setColor(color.WHITE);
		g2.fillOval(12, 20, 13, 13);
		g2.setColor(Color.BLACK);
		g2.drawOval(12, 20, 13, 13);
		g2.setColor(Color.BLUE);
		g2.fillOval(18, 26, 3, 3);

		/**
		 *  Auge2
		 */
		g2.setColor(color.WHITE);
		g2.fillOval(27, 20, 13, 13);
		g2.setColor(Color.BLACK);
		g2.drawOval(27, 20, 13, 13);
		g2.setColor(Color.BLUE);
		g2.fillOval(33, 26, 3, 3);

		/**
		 *  Mund
		 */
		g2.setColor(color.BLACK);
		g2.fillOval(25, 40, 10, 4);
		// Transformation zuruecksetzen
		g2.setTransform(atOriginal);

		/**
		 *  Reset Verschiebung des Objekts
		 */
		g2.translate(-positionX, -positionY);
		
	}
}