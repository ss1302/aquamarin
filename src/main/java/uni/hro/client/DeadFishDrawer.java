package uni.hro.client;

/**
 *
 */

import uni.hro.model.Fish;
import uni.hro.model.Food;
import uni.hro.model.Point;
import uni.hro.model.WorldEntity;
import uni.hro.model.neurons.InputNeuron;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.Map;

/**
 * @author Joerg
 * 
 *         Diese Klasse bestimmt, wie ein toter Fisch gezeichnet wird.
 */

class DeadFishDrawer {

	/**
	 * Diese Methode bestimmt, wie ein toter Fisch gezeichnet wird.
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * @param fish
	 *            Der zu zeichnende Fisch
	 */
	public static void draw(Graphics graphics, Fish fish) {

		/**
		 * Anpassen der Position, damit der Fisch genau in der Hit-Box
		 * gezeichnet wird
		 */
		int positionX = (int) fish.getX() - 10;
		int positionY = (int) fish.getY() - 5;

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Fisches. Ueber den
		 * SizefactorX wird der Fisch gespiegelt, wenn er nach links gucken
		 * soll.
		 */
		
		double sizefactorX = (fish.looksLeft()) ? -0.2 : 0.2;
		double sizefactorY = 0.2;

		Color currentColor = fish.getCurrentColor();
		Graphics2D g2 = (Graphics2D) graphics;

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Fisches
		 * 
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);
		if (sizefactorX < 0) {
			g2.translate(-100 * sizefactorX, 0);
		}
		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/**
		 * Bauchflosse
		 */
		/**
		 * int[] xPoints = { 50, 50, 90 }; int[] yPoints = { 40, 60, 40 }; int
		 * nPoints = 3; g2.setColor(Color.white); g2.fillPolygon(xPoints,
		 * yPoints, nPoints); g2.setColor(Color.BLACK); g2.drawPolygon(xPoints,
		 * yPoints, nPoints);
		 **/

		/**
		 * Schwanzflosse
		 */
		int[] xPoints2 = { 0, 20, 20, 0 };
		int[] yPoints2 = { 0, 20, 30, 50 };
		int nPoints2 = 4;
		g2.setColor(Color.white);
		g2.fillPolygon(xPoints2, yPoints2, nPoints2);
		g2.setColor(Color.BLACK);
		g2.drawPolygon(xPoints2, yPoints2, nPoints2);

		/**
		 * Kopf
		 */
		g2.setColor(Color.white);
		g2.fillArc(20, 0, 80, 50, 270, 180);
		g2.setColor(Color.BLACK);
		g2.drawArc(20, 0, 80, 50, 270, 180);

		/**
		 * Koerper
		 */
		g2.setColor(Color.white);
		g2.fillRect(20, 24, 80, 4);
		g2.fillRect(20,10,2,34);
		g2.fillRect(30,8,2,38);
		g2.fillRect(40,5,2,40);
		g2.fillRect(50,0,2,50);
	
		/**
		 * Auge
		 * 
		 */
		g2.setColor(Color.BLACK);
		g2.fillOval(70, 20, 10, 10);
		g2.setTransform(atOriginal);

		/**
		 * Reset Verschiebung des Fisches
		 */
		g2.translate(-positionX, -positionY);

	}

}