/**
 * 
 */
package uni.hro.client;

import java.awt.Color;
import java.util.Random;

import uni.hro.model.Fish;
import uni.hro.model.Food;
import uni.hro.model.Kraken;
import uni.hro.model.Plant;
import uni.hro.model.Stone;
import uni.hro.model.WorldEntity;

/**
 * @author Columbus
 *
 *	In dieser Klasse werden Farben f�r die verschiedenen Objekte des Aquariums generiert. 
 */
public class ColorGenerator {

	/**
	 * In dieser Methode werden Farben f�r die verschiedenen Objekte des Aquariums generiert. 
	 * @param worldEntity
	 * 	Abh�ngig von der Klasse der WorldEntity werden verschiedene Farbbereiche generiert.
	 * @return
	 */
	public static Color mixRandomColor(WorldEntity worldEntity) {

		if (worldEntity instanceof Fish) {
			Color randomColor = new Color(generateRandom(0, 150), generateRandom(150, 255),
					generateRandom(150, 255));
			return randomColor;
		}
		
		if (worldEntity instanceof Kraken) {
			Color randomColor = new Color(generateRandom(200, 255), generateRandom(0, 150),
					generateRandom(0, 150));
			return randomColor;
		}

		if (worldEntity instanceof Stone) {
			Color randomColor = new Color(generateRandom(120, 130), generateRandom(100, 150),
					generateRandom(100,150));
			return randomColor;
		}
		
		if (worldEntity instanceof Food) {
			Color randomColor = new Color(generateRandom(200, 255), generateRandom(200, 255),
					generateRandom(200,255));
			return randomColor;
		}
		
		if (worldEntity instanceof Plant) {
			Color randomColor = new Color(generateRandom(0, 100), generateRandom(150, 255),
					generateRandom(50,150));
			return randomColor;
		}
		return null;
	}

	/**
	 * Erzeugt eine Integer-Zufallszahl zwischen min und max.
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}
}
