package uni.hro.client;

/**
 * 
 */

import javax.swing.*;

import uni.hro.model.Plant;

import java.awt.*;
import java.awt.geom.*;
import java.util.Random;

/**
 * @author Joerg
 *
 *         Diese Klasse bestimmt, wie eine Pflanze gezeichnet wird.
 */

public class PlantDrawer {

	/**
	 * Diese Methode bestimmt, wie eine Pflanze gezeichnet wird.
	 * 
	 * @param graphics
	 *            Grafik des AquariumJPanel
	 * @param kraken
	 *            Die zu zeichnende Pflanze
	 */
	public static void draw(Graphics g, Plant plant) {

		Graphics2D g2 = (Graphics2D) g;

		int positionX = (int) plant.getX();
		int positionY = (int) plant.getY();

		/**
		 * Pflanzen sind aus aufeinander sitzenden Teilen (Segmenten aufgebaut)
		 * numberOfSegments bestimmt also die Hoehe der Pflanze
		 */
		int numberOfSegments = plant.getNumberofSegments();
		int[] xPoints = plant.getxLeafGeometriePoints();
		int[] yPoints = plant.getyLeafGeometriePoints();

		/**
		 * SizefactorX / -Y bestimmen die Zeichengroesse des Kraken.
		 */
		double sizefactorX = 0.3;
		double sizefactorY = 0.3;

		/**
		 * Drehwinkel eines Pflanzenteils zu einer lotrechten Pflanze
		 */
		double plantAngle = plant.getCurrentAngle();
		Color color = plant.getCurrentColor();

		/**
		 * Verschiebung des Objekts
		 */
		g2.translate(positionX, positionY);

		/**
		 * Usprungszustand des Objekts
		 */
		AffineTransform atOriginal = g2.getTransform();

		/**
		 * Skalierung des Objekts
		 */
		AffineTransform at = new AffineTransform();
		at.setToScale(sizefactorX, sizefactorY);
		if (sizefactorX < 0) {
			g2.translate(-100 * sizefactorX, 0);
		}
		/**
		 * Transformationen anwenden
		 */
		g2.transform(at);

		/**
		 * Antialiasing aktiviert
		 */
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		/**
		 * Zeichnen eines Astes der Pflanze
		 */
		drawABranch(g2, numberOfSegments, xPoints, yPoints, plantAngle, color);

		/**
		 * Transformation zuruecksetzen
		 */
		g2.setTransform(atOriginal);

		/**
		 * Reset Verschiebung des Objekts
		 */
		g2.translate(-positionX, -positionY);

	}

	/**
	 * Bestimmt, wie ein einzelner Zweig einer Pflanze gezeichnet wird.
	 * 
	 * @param g2
	 *            Grafik des AquariumJPanels
	 * @param numberofSegments
	 *            Anzahl der Pflanzenteile = Hoehe der Pflanze
	 * @param xPoints
	 *            X-Werte der Geometriepunkte fuer ein einzelnes Blatt
	 * @param yPoints
	 *            Y-Werte der Geometriepunkte fuer ein einzelnes Blatt
	 * @param plantAngle
	 *            Drehwinkel eines Pflanzenteils zu einer lotrechten Pflanze
	 * @param color
	 *            Zeichenfarbe des Pflanzenteils
	 */
	private static void drawABranch(Graphics2D g2, int numberofSegments, int[] xPoints, int[] yPoints,
			double plantAngle, Color color) {

		int endOfBranchX = 0;
		int endofBranchY = -100;

		int iterator = 0;
		while (iterator <= numberofSegments) {
			drawABranchPart(g2, plantAngle, endOfBranchX, endofBranchY, xPoints, yPoints, color);
			iterator++;
		}

		/**
		 * Zeichnet ein gedrehtes BLatt am hoechsten Punkt der Pflanze
		 */
		AffineTransform at = new AffineTransform();
		at.rotate(45);
		g2.transform(at);
		drawALeaf(g2, true, 0, 0, xPoints, yPoints, color);
		AffineTransform at2 = new AffineTransform();
		at.rotate(plantAngle);
		g2.transform(at2);
	}

	/**
	 * 
	 * Bestimmt, wie ein einzelnes Segment eines Zweiges gezeichnet wird.
	 * 
	 * @param g2
	 *            Grafik des AquariumJPanels
	 * @param endOfBranchX
	 *            X-Koordinate des Endes des vorherigen Pflanzensegments
	 * @param endofBranchY
	 *            Y-Koordinate des Endes des vorherigen Pflanzensegments
	 * @param color
	 *            Zeichenfarbe des Pflanzenteils
	 */
	private static void drawABranchPart(Graphics2D g2, double plantAngle, int endOfBranchX, int endofBranchY,
			int[] xPoints, int[] yPoints, Color color) {

		/**
		 * Pflanzenteil wird um einen bestimmten Winkel gedreht.
		 */
		AffineTransform at = new AffineTransform();
		at.rotate(plantAngle);
		g2.transform(at);

		/**
		 * Ein Stengel wird gezeichnet (das Rechteckt) Links und rechts des
		 * Stengels werden je 2 Blaetter gezeichnet.
		 */
		g2.setColor(color);
		g2.fillRect(0, endofBranchY, endOfBranchX + 5, -endofBranchY);
		g2.setColor(Color.BLACK);
		g2.drawRect(0, endofBranchY, endOfBranchX + 5, -endofBranchY);

		drawALeaf(g2, true, 0, 0, xPoints, yPoints, color);
		drawALeaf(g2, true, 0, -50, xPoints, yPoints, color);

		g2.translate(0, 50);

		drawALeaf(g2, false, 0, 0, xPoints, yPoints, color);
		drawALeaf(g2, false, 0, -50, xPoints, yPoints, color);

		g2.translate(0, -50);
	}

	/**
	 * 
	 * @param g2
	 *            Grafik des AquariumJPanels
	 * @param left
	 *            Ist das Blatt nach links gedreht?
	 * @param TranslatePositionX
	 *            Verschiebung des Blatts in X-Richtung
	 * @param TranslatePositionY
	 *            Verschiebung des Blatts in Y-Richtung
	 * @param xPoints
	 *            X-Werte zum Zeichnen eines einzelnen Blatts
	 * @param yPoints
	 *            Y-Werte zum Zeichnen eines einzelnen Blatts
	 * @param color
	 *            Zeichenfarbe des Pflanzenteils
	 */
	private static void drawALeaf(Graphics2D g2, boolean left, int TranslatePositionX, int TranslatePositionY,
			int[] xPoints, int[] yPoints, Color color) {

		if (left == true) {
			g2.translate(TranslatePositionX, TranslatePositionY);
			AffineTransform at = new AffineTransform();
			at.rotate(-45);

			/**
			 * Transformationen anwenden
			 */
			g2.transform(at);

			/**
			 * Blattpunkte malen
			 */
			int[] xPoints2 = xPoints;
			int[] yPoints2 = yPoints;

			int nPoints2 = xPoints2.length;
			g2.setColor(color);
			g2.fillPolygon(xPoints2, yPoints2, nPoints2);
			g2.setColor(Color.BLACK);
			g2.drawPolygon(xPoints2, yPoints2, nPoints2);

			AffineTransform at2 = new AffineTransform();
			at2.rotate(45);

			/**
			 * Transformationen anwenden
			 * 
			 */
			g2.transform(at2);
			/**
			 * Blattpunkte malen
			 */
		} else {
			g2.translate(TranslatePositionX, TranslatePositionY);
			AffineTransform at = new AffineTransform();
			at.rotate(45);

			/**
			 * Transformationen anwenden
			 */
			g2.transform(at);

			/**
			 * Blattpunkte malen
			 */
			int[] xPoints2 = xPoints;
			int[] yPoints2 = yPoints;

			int nPoints2 = xPoints2.length;
			g2.setColor(color);
			g2.fillPolygon(xPoints2, yPoints2, nPoints2);
			g2.setColor(Color.BLACK);
			g2.drawPolygon(xPoints2, yPoints2, nPoints2);

			AffineTransform at2 = new AffineTransform();
			at2.rotate(-45);

			/**
			 * Transformationen anwenden
			 */
			g2.transform(at2);

		}

	}

	/**
	 * 
	 * Erzeugt X-Werte zum Zeichnen eines einzelnen Blatts.
	 *
	 * @return
	 */
	public static int[] createRandomizedGeometrieValuesX() {
		int[] xPoints = { 0, -10, -20, -30, -10, 0, 10, 30, 20, 10, 0 };
		return xPoints;
	}

	/**
	 * 
	 * Erzeugt zufaellige Y-Werte zum Zeichnen eines einzelnen Blatts.
	 *
	 * @return
	 */
	public static int[] createRandomizedGeometrieValuesY() {

		int a, b, c, d, e;
		a = -20 + generateRandom(-20, 20);
		b = -40 + generateRandom(-20, 20);
		c = -60 + generateRandom(-20, 20);
		d = -80 + generateRandom(-30, 20);
		e = -100 + generateRandom(-40, 20);
		/**
		 * Blaetter sind symmetrisch,
		 */
		int[] yPoints = { 0, a, b, c, d, e, d, c, b, a, 0 };

		return yPoints;
	}

	/**
	 * Erzeugt eine Integer-Zufallszahl zwischen min und max.
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int generateRandom(int min, int max) {
		Random rn = new Random();
		int randomvalue = rn.nextInt(max - min + 1) + min;
		return randomvalue;
	}
}