package uni.hro.networking;

import com.sun.istack.internal.NotNull;
import de.mein.auth.data.MeinAuthSettings;
import de.mein.auth.data.MeinRequest;
import de.mein.auth.data.access.CertificateManager;
import de.mein.auth.data.db.Certificate;
import de.mein.auth.data.db.ServiceJoinServiceType;
import de.mein.auth.service.MeinAuthService;
import de.mein.auth.service.MeinBoot;
import de.mein.auth.socket.process.reg.IRegisterHandler;
import de.mein.auth.socket.process.reg.IRegisterHandlerListener;
import de.mein.auth.socket.process.reg.IRegisteredHandler;
import de.mein.auth.tools.N;
import de.mein.auth.tools.WaitLock;
import uni.hro.networking.service.AquaBootLoader;
import uni.hro.networking.service.AquaService;

import java.io.File;
import java.util.List;

/**
 * Created by xor on 5/13/17.
 */
public abstract class Aqua<T extends AquaService> {

    private final int portCert;
    private final String name;
    private final int port;
    private final File directory;
    protected AquaService aquaService;
    private MeinAuthService meinAuthService;


    public T getAquaService() {
        return (T) aquaService;
    }

    public MeinAuthService getMeinAuthService() {
        return meinAuthService;
    }

    public Aqua(@NotNull File directory, @NotNull String name, @NotNull int port, @NotNull int portCert) {
        this.port = port;
        this.portCert = portCert;
        this.name = name;
        this.directory = directory;
    }

    /**
     * starts a {@link MeinAuthService} and creates a {@link AquaService} in it. blocks until it is done.
     * {@link MeinAuthService} is configured so it accepts all registration requests
     * and grants access to the {@link AquaService} for everyone.
     *
     * @return
     * @throws Exception
     */
    public MeinAuthService startAqua() throws Exception {
        CertificateManager.deleteDirectory(directory);
        MeinBoot.addBootLoaderClass(AquaBootLoader.class);

        // we want accept all registration attempts automatically
        IRegisterHandler allowRegisterHandler = new IRegisterHandler() {
            @Override
            public void acceptCertificate(IRegisterHandlerListener listener, MeinRequest request, Certificate myCertificate, Certificate certificate) {
                listener.onCertificateAccepted(request, certificate);
            }

            @Override
            public void onRegistrationCompleted(Certificate partnerCertificate) {

            }
        };
        // we want to allow every registered Certificate to talk to all available Services
        IRegisteredHandler registeredHandler = (meinAuthService, registered) -> {
            List<ServiceJoinServiceType> services = meinAuthService.getDatabaseManager().getAllServices();
            for (ServiceJoinServiceType serviceJoinServiceType : services) {
                meinAuthService.getDatabaseManager().grant(serviceJoinServiceType.getServiceId().v(), registered.getId().v());
            }
        };

        // craft some delicious settings
        MeinAuthSettings settings = new MeinAuthSettings().setPort(port).setDeliveryPort(portCert)
                .setBrotcastListenerPort(9966).setBrotcastPort(9966)
                .setWorkingDirectory(directory).setName("MA1").setGreeting("greeting1");


        WaitLock waitLock = new WaitLock().lock();
        MeinBoot meinBoot = new MeinBoot(settings);
        meinBoot.boot().done(ma1 -> N.r(() -> {
            meinAuthService = ma1;
            meinAuthService.addRegisteredHandler(registeredHandler);
            meinAuthService.addRegisterHandler(allowRegisterHandler);
            System.out.println("AquaServer.main.server.is.up");
            AquaBootLoader aquaBootLoader = (AquaBootLoader) MeinBoot.getBootLoader(meinAuthService, new AquaBootLoader().getName());
            if (isServer())
                aquaService = aquaBootLoader.newServerService(name);
            else
                aquaService = aquaBootLoader.newClientService(name);
            waitLock.unlock();
        })).fail(Throwable::printStackTrace);
        waitLock.lock();
        return meinAuthService;
    }

    abstract boolean isServer();
}
