package uni.hro.networking.service;

import de.mein.auth.data.IPayload;
import de.mein.auth.data.db.Certificate;
import de.mein.auth.jobs.Job;
import de.mein.auth.jobs.ServiceMessageHandlerJob;
import de.mein.auth.service.MeinAuthService;
import de.mein.auth.socket.process.val.MeinServicesPayload;
import de.mein.auth.socket.process.val.Request;
import de.mein.auth.tools.N;
import uni.hro.model.Bucket;
import uni.hro.model.Fish;
import uni.hro.networking.AquaServer;
import uni.hro.networking.AquaStrings;
import uni.hro.networking.job.StartSimulationJob;
import uni.hro.server.ServerGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by xor on 5/13/17.
 */
public class AquaServerService extends AquaService {

    private Map<Long, MeinServicesPayload> clientDetailsMap = new HashMap<>();
    private Set<Fish> availableFishes = new HashSet<>();
    private ServerGui gui;

    public AquaServerService(MeinAuthService meinAuthService, File serviceInstanceWorkingDirectory) {
        super(meinAuthService, serviceInstanceWorkingDirectory);
        this.gui = new ServerGui();
        this.gui.setStartListener(new StartListener());
    }

    public void startSimulation() {
        // tell every client to start simulation
        clientDetailsMap.forEach((certId, meinServicesPayload) -> N.r(() -> {
            meinAuthService.connect(certId).done(validationProcess -> N.r(() -> {
                validationProcess.message(meinServicesPayload.getServices().get(0).getUuid().v(), AquaStrings.INTENT_START_SIMULATION, null);
            }));
        }));
        // simulate for yourself
        addJob(new StartSimulationJob());
    }


    @Override
    public void handleMessage(IPayload payload, Certificate partnerCertificate, String intent) {
        addJob(new ServiceMessageHandlerJob().setMessage(payload).setIntent(intent).setPartnerCertificate(partnerCertificate));
    }

    @Override
    public void connectionAuthenticated(Certificate partnerCertificate) {
        // something connected
        System.out.println("AquaServerService.connectionAuthenticated");
    }

    @Override
    public void onMeinAuthIsUp() {

    }

    @Override
    protected void workWorkWork(Job job) {
        if (job instanceof ServiceMessageHandlerJob) {
            ServiceMessageHandlerJob messageHandlerJob = (ServiceMessageHandlerJob) job;
            if (messageHandlerJob.isRequest()) {
                Request request = messageHandlerJob.getRequest();
                if (request.getIntent().equals(AquaStrings.INTENT_REG_AS_CLIENT)) {
                    MeinServicesPayload payload = (MeinServicesPayload) request.getPayload();
                    clientDetailsMap.put(((ServiceMessageHandlerJob) job).getPartnerCertificate().getId().v(), payload);
                    request.resolve(null);
                }
            } else if (messageHandlerJob.getIntent() != null) {
                if (messageHandlerJob.getIntent().equals(AquaStrings.INTENT_TRANSFER_BUCKET)) {
                    Bucket bucket = (Bucket) messageHandlerJob.getPayLoad();
                    availableFishes.addAll(bucket.getFishes());
                }
            }
        }
    }


    class StartListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
                // Testing only! This buttons should start the simulation on all connected PI's.
                gui.connectClient("localhost");
                startSimulation();
                gui.setVisible(false);
        }
    }
}
