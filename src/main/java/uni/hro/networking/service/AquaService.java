package uni.hro.networking.service;

import de.mein.auth.data.db.Certificate;
import de.mein.auth.jobs.Job;
import de.mein.auth.jobs.ServiceMessageHandlerJob;
import de.mein.auth.service.MeinAuthService;
import de.mein.auth.service.MeinService;
import de.mein.auth.socket.process.transfer.MeinIsolatedProcess;
import de.mein.auth.socket.process.val.Request;
import de.mein.auth.tools.WaitLock;
import de.mein.core.serialize.serialize.tools.OTimer;
import uni.hro.client.Aquarium;
import uni.hro.model.World;
import uni.hro.networking.job.SimulateJob;
import uni.hro.networking.job.StartSimulationJob;
import uni.hro.networking.job.StopSimulatingJob;
import uni.hro.simulation.Simulation;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by xor on 5/12/17.
 */
public abstract class AquaService extends MeinService {
	
	private static int framteTime = 0;
	protected Simulation simulation;
	protected Aquarium aquarium;
	protected World world;
	private boolean simulating = false;
	private OTimer oTimer = new OTimer("Simulation duration");
	private OTimer fps = new OTimer("fsp").start();
	private Timer timer = new Timer("el simulatore");

	public int generationNumber = 1;
	public int jumpToGeneration  = 1;
	
	public AquaService(MeinAuthService meinAuthService, File serviceInstanceWorkingDirectory) {
		super(meinAuthService, serviceInstanceWorkingDirectory);
	}

	@Override
	public void handleRequest(Request request) throws Exception {
		addJob(new ServiceMessageHandlerJob().setRequest(request));
	}

	@Override
	public void handleCertificateSpotted(Certificate partnerCertificate) {
		// not needed
	}

	@Override
	public void onIsolatedConnectionEstablished(MeinIsolatedProcess isolatedProcess) {
		// not needed
	}

	@Override
	protected final void workWork(Job job) throws Exception {
		if (job instanceof SimulateJob) {
			simulate();
			if (simulating)
				addJob(new SimulateJob());
		} else if (job instanceof StopSimulatingJob) {
			simulating = false;
		} else if (job instanceof StartSimulationJob) {

			simulating = true;
			simulation = new Simulation(15, 2, 2, 1, 480, 320); // TODO: als
																	// config-Datei
			world = simulation.getWorld();
			aquarium = new Aquarium(world);
			simulation.prepare();
			addJob(new SimulateJob());
		} else {

			workWorkWork(job);

		}
	}

	protected abstract void workWorkWork(Job job);

	protected void simulate() {
		oTimer.start();
		boolean hasFinished = simulation.simulate();

		/**
		 * 
		 */
		jumpToGeneration=jumpToGeneration+aquarium.getaquariumJPanel().getJumpNumberOfGenerations();
		aquarium.getaquariumJPanel().setJumpNumberOfGenerations(0);
		if (generationNumber >= jumpToGeneration) {
			
			framteTime=10;
			aquarium.repaint();
		}
		else
		{
			framteTime=0;
		}

		oTimer.stop();
		if (oTimer.getDurationInMS() < framteTime) {
			WaitLock waitLock = new WaitLock().lock();
			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					waitLock.unlock();
				}
			}, framteTime - oTimer.getDurationInMS());
			waitLock.lock();
			waitLock.unlock();
		}
		oTimer.reset();
		if (!hasFinished) {
			// System.out.println("fps.simulation: "+fps.fps());
			addJob(new SimulateJob());
		} else {
			
		
			generationNumber++;
          //	System.out.println("Generation Nummer: "+ generationNumber);
			simulation.evolve();
			simulation.prepare();
			
			fastforward();
			
			addJob(new SimulateJob());
			
		}
	}

	/**
	 * 
	 */
	private void fastforward() {
		
		aquarium.getaquariumJPanel().setFastforward(true);
		aquarium.setGeneration(generationNumber);
		aquarium.refreshLabel();
		aquarium.repaint();
	
	}

	@Override
	protected ExecutorService createExecutorService(ThreadFactory threadFactory) {
		return Executors.newCachedThreadPool(threadFactory);
	}

}
